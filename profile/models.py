import logging
import datetime
from phonenumber_field.modelfields import PhoneNumberField
from userena.models import UserenaBaseProfile
from profile.managers import ErinireManager

from django.contrib.auth.models import User
from django.db import models

from taggit.managers import TaggableManager

from django.utils.translation import ugettext as _
from profile.baseerinire import ErinireObject

class ErinireProfile(UserenaBaseProfile, ErinireObject):
    #Default class
    #created = models.DateTimeField()
    #updated = models.DateTimeField()
    #status = models.CharField(max_length=10)
    about_me = models.TextField(_('about me'), blank=True)

    user = models.ForeignKey(User, unique=True)

    username = models.CharField(max_length=100 )
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    website = models.CharField(max_length=200, blank=True)
    email = models.EmailField()
    summary = models.TextField(_('summary'), blank=True)
    industry = models.CharField(max_length=100,  blank=True)
    expertise = TaggableManager(blank=True)
    #group = models.ForeignKey(ErinireGroup)
    #information = models.ManyToManyField(ErinireObject,
    #                         related_name="user_created_intel", null=True)
    phone = PhoneNumberField(blank=True, null=True )


    objects = ErinireManager()

    @staticmethod
    def create_user(username, email, password, **kwargs):
        print kwargs
        created, updated = (datetime.datetime.now(),
                            datetime.datetime.now())
        #kwargs['created'] = created
        #kwargs['updated'] = updated
        new_user = ErinireProfile.objects.create_user( username, email, password, **kwargs)
        logging.debug("Created a new user: %s %s"%(username, email))
        #ep = ErinireProfile(**kwargs)
        #ep.save()
        return new_user

    def __unicode__(self):
        return '%(username)s Profile' % {'username': self.user.username}

    def get_mugshot_url(self):
        return None

    def get_full_name_or_username(self):

        name = self.first_name + u' ' + self.last_name
        if name == u'':
            name = self.username
        return name

    def can_view_profile(self, user):
        # TODO add logic to check user role, and then determine if user can
        # view this profile
        return True