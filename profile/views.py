import logging
import traceback
from userena.views import ExtraContextTemplateView
from erinire import settings
from profile.forms import ErinireAuthenticationForm, ErinireSignupForm
from profile.models import ErinireProfile
from util.baseview import BaseView, get, post
from util.decorators import secure_required_cls

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.core.context_processors import csrf
from django.shortcuts import redirect
from django.template import RequestContext
from django.utils.translation import ugettext as _

from coffin.shortcuts import render_to_response

class ActivateUser(BaseView):
    @get
    @secure_required_cls
    def handle_get(self, request, username='', activation_key=''):
        logging.debug("Handling %s activation with %s key"%(username,
                                                            activation_key[:10]))
        user = ErinireProfile.objects.activate_user(username, activation_key)
        if user:
            logging.debug("User %s account activated."%(username))
            return redirect('userena_signin')
        return ExtraContextTemplateView.as_view('userena/activate_fail.html',
            extra_context={})(request)

class UserenaWrapperSignup(BaseView):
    success_url =  getattr(settings, 'SIGNUP_REDIRECT', 'http://www.google.com')
    signup_template = getattr(settings, 'SIGNUP_TEMPLATE', 'attacksubmission/signup_form.html')
    @get
    @secure_required_cls
    def handle_get(self, request ):
        form = ErinireSignupForm()
        req_context = {'request':request,'title': 'Erinire Registration Page', 'form': form}
        req_context.update(csrf(request))
        return render_to_response(self.signup_template, req_context,
            context_instance=RequestContext(request))
    @post
    @secure_required_cls
    def handle_post(self, request):
        form = ErinireSignupForm(request.POST, request.FILES)
        req_context = {'request':request,'title': 'Erinire Registration Page', 'form': form}
        user = None

        if form.is_valid():
            logging.debug("Form is valid")
            try:
                user = form.save()
                # Send the signup complete signal
                #userena_signals.signup_complete.send(sender=None,
                #                                     user=user)
                req_context = {'request':request,'form':None, 'user':user,'title': 'Erinire Registration Complete'}
                req_context.update(csrf(request))
                return render_to_response('erinire/signup_submitted.html',
                    req_context, context_instance=RequestContext(request))
            except:
                traceback.print_exc()

        req_context.update(csrf(request))
        return render_to_response(self.signup_template, req_context,
            context_instance=RequestContext(request))

class SigninView(BaseView):
    REDIRECT = '/%s/'
    @get
    @secure_required_cls
    def get_submission_page(self, request):
        form = ErinireAuthenticationForm()
        req_context = {'request':request, 'title': _('User Sign-in'), 'form': form}
        req_context.update(csrf(request))
        return render_to_response('erinire/signin_form.html', req_context,
            context_instance=RequestContext(request))

    @post
    @secure_required_cls
    def post_submission_page(self, request):
        auth_form = ErinireAuthenticationForm(request.POST, request.FILES )
        if auth_form.is_valid():
            logging.debug("Form passed validation")
            username = auth_form.cleaned_data['username']
            password = auth_form.cleaned_data['password']
            #rememberme = auth_form.cleaned_data['remember_me']
            user = authenticate(username=username, password=password)
            if not user is None and user.is_active:
                login(request, user)
                #if rememberme:
                #    request.session.set_expiry(userena_settings.USERENA_REMEMBER_ME_DAYS[1] * 86400)
                #else:
                request.session.set_expiry(0)
                messages.success(request, _('You have been signed in.'),
                    fail_silently=False)
                #return redirect('userena_profile_detail')
                return redirect('submission')
        logging.debug("Invalid user?")
        req_context = {'request':request,'title': _('User Sign-in'), 'form': auth_form}
        req_context.update(csrf(request))
        logging.debug("%s", str(auth_form))
        return render_to_response('erinire/signin_form.html', req_context,
            context_instance=RequestContext(request))
