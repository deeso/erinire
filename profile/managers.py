import re, logging
from django.contrib.auth.models import (SiteProfileNotAvailable,
                                        User, UserManager,
                                        Permission, AnonymousUser,)
from userena.managers import UserenaManager
from django.db.models import Q, get_model
from userena.models import UserenaSignup
from userena import signals as userena_signals
from guardian.shortcuts import assign, get_perms
import erinire.settings as settings

from hashlib import sha512

from userena import settings as userena_settings

KEY_RE = re.compile('^[a-f0-9]{10}$')

ASSIGNED_PERMISSIONS = {
    'profile':
        (('view_profile', 'Can view profile'),
         ('change_profile', 'Can change profile'),
         ('delete_profile', 'Can delete profile')),
    'user':
        (('change_user', 'Can change user'),
         ('delete_user', 'Can delete user'))
}


# TODO rewrite this class when I have time
# blatantly taken from the Userena source
class ErinireManager(UserenaManager):
    def __init__(self):
        super(ErinireManager, self).__init__()

    def get_visible_profiles(self, user=None):
        profiles = self.all()

        filter_kwargs = {'user__is_active': True}

        profiles = profiles.filter(**filter_kwargs)
        if user and isinstance(user, AnonymousUser):
            profiles = profiles.exclude(Q(privacy='closed') | Q(privacy='registered'))
        else: profiles = profiles.exclude(Q(privacy='closed'))
        return profiles


    def activate_user(self, username, activation_key):
        """
        Activate an :class:`User` by supplying a valid ``activation_key``.

        If the key is valid and an user is found, activates the user and
        return it. Also sends the ``activation_complete`` signal.

        :param username:
            String containing the username that wants to be activated.

        :param activation_key:
            String containing the secret SHA1 for a valid activation.

        :return:
            The newly activated :class:`User` or ``False`` if not successful.

        """
        if KEY_RE.search(activation_key):
            try:
                userena = UserenaSignup.objects.get(user__username=username,
                    activation_key=activation_key)
            except self.model.DoesNotExist:
                return False
            if not userena.activation_key_expired():
                userena.activation_key = userena_settings.USERENA_ACTIVATED
                user = userena.user
                user.is_active = True
                userena.save(using=self._db)
                user.save(using=self._db)

                # Send the activation_complete signal
                userena_signals.activation_complete.send(sender=None,
                    user=user)
                return user
        return False

    def create_user(self, username, email, password, active=False, send_email=True, **kwargs):
        new_user = User.objects.create_user(username, email, password)
        new_user.is_active = active
        new_user.save()

        #new_user.username = new_user.username.encode('utf-8')

        activation_key = sha512(new_user.username).hexdigest()[45:55]
        userena_profile = UserenaSignup.objects.create(user=new_user, activation_key=activation_key)
        userena_profile.send_activation_email()
        userena_profile.save()
        model_profile = get_model(*settings.AUTH_PROFILE_MODULE.split('.'))
        if model_profile is None:
            raise SiteProfileNotAvailable

        new_profile = model_profile(user=new_user,
            username=username, email=email, **kwargs)

        new_profile.save()

        # Give permissions to view and change profile
        for perm in ASSIGNED_PERMISSIONS['profile']:
            assign(perm[0], new_user, new_profile)

        # Give permissions to view and change itself
        for perm in ASSIGNED_PERMISSIONS['user']:
            assign(perm[0], new_user, new_user)

        logging.debug("New user created successfully?")
        return new_profile
