import logging
from django.conf import settings
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from form_utils.forms import BetterForm, BetterModelForm
from util.widgets import (TelephoneInput, WebsiteInput)
from attacksubmission.models import (ErinireProfile)


from taggit.forms import TagField
from userena import settings as userena_settings


USERNAME_RE = r'^[\.\w]+$'
NAME_RE = r'^[a-zA-Z]+$'

class ErinireSignupForm(BetterForm):
    #class Meta:
    #    model = ErinireProfile

    username = forms.RegexField(regex=USERNAME_RE,
        max_length=30,
        widget=forms.TextInput(),
        label=_("Username"),
        error_messages={'invalid': _('Username must contain only letters, numbers, dots and underscores.')})
    first_name = forms.RegexField(regex=NAME_RE, required = False,
        max_length=63,
        widget=forms.TextInput(),
        label=_("First Name"),
        error_messages={'invalid': _('First name must contain only letters.')})
    last_name = forms.CharField( max_length=63, required = False,
        widget=forms.TextInput(),
        label=_("Last Name"),
        error_messages={'invalid': _('Last name must contain only letters.')})
    phone = forms.CharField(  max_length=10, required = False,
        widget=TelephoneInput(),
        label=_("Phone Number"),
        error_messages={'invalid': _('Enter a valid phone number.')})


    industry = forms.CharField( max_length=50, required = False,
        widget=forms.TextInput(),
        label=_("Industry"),
        error_messages={'invalid': _('Last name must contain only letters.')})
    email = forms.EmailField( label=_("Email"),
        widget=forms.TextInput(attrs={'input_type':'email'},),
        error_messages={'invalid': _('This email address has already been used.')})
    website  = forms.URLField( max_length=100, required = False,
        widget=WebsiteInput(),
        label=_("Website"),
        error_messages={'invalid': _('Website must start with "http://".')})
    industry = forms.CharField(max_length=100, required = False)
    expertise = TagField(required = False)

    password1 = forms.CharField(widget=forms.PasswordInput(
        render_value=False),
        label=_("Create password"))
    password2 = forms.CharField(widget=forms.PasswordInput(
        render_value=False),
        label=_("Repeat password"))

    def clean(self):
        if any(self.errors):
            return
        for field in self:
            data= field.value
            if not data and field.field.required:
                raise forms.ValidationError, "Please Complete the Required Fields"
        self.clean_email()
        #logging.debug("Email passed check.")
        self.cleaned_data['password'] = self.clean_password()
        #logging.debug("Password passed check.")
        self.clean_user()
        #logging.debug("Username passed check.")
        return self.cleaned_data


    def clean_password(self):
        cnt_digits = lambda x: sum([ 1 for i in x if i.isdigit()])
        cnt_upper = lambda x: sum([ 1 for i in x if i.isdigit()])
        cnt_lower = lambda x: sum([ 1 for i in x if i.isdigit()])
        min_length = getattr(settings, "ACCOUNT_PASSWORD_LENGTH", 6)
        special_char_req = getattr(settings, "ACCOUNT_PASSWORD_NUM_SPECIAL", 1)
        upper_char_req = getattr(settings, "ACCOUNT_PASSWORD_NUM_UPPER", 1)
        lower_char_req = getattr(settings, "ACCOUNT_PASSWORD_NUM_LOWER", 1)
        digit_char_req = getattr(settings, "ACCOUNT_PASSWORD_NUM_DIGIT", 1)
        error_msg = "Password must be " +\
                    "%d characters long with %d special,"%(min_length, special_char_req) +\
                    " %d digits, %d upper case, and %d"%(digit_char_req,
                                                         upper_char_req,
                                                         lower_char_req) +\
                    " lower case characters required."

        if self.cleaned_data['password1'] != self.cleaned_data['password2']:
            raise forms.ValidationError(_("The passwords do not match."))

        cnt_d = cnt_digits(self.cleaned_data['password1'])
        cnt_l = cnt_lower(self.cleaned_data['password1'])
        cnt_u = cnt_upper(self.cleaned_data['password1'])
        cnt_s = len(self.cleaned_data['password1']) - sum([cnt_d, cnt_l, cnt_u])

        if len(self.cleaned_data['password1']) < min_length:
            raise forms.ValidationError(_(error_msg))
        if cnt_d < digit_char_req:
            raise forms.ValidationError(_(error_msg))
        if cnt_u < upper_char_req:
            raise forms.ValidationError(_(error_msg))
        if cnt_s < special_char_req:
            raise forms.ValidationError(_(error_msg))
        return self.cleaned_data['password1']

    def clean_email(self):
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email is already in use"))
        return self.cleaned_data['email']

    def clean_user(self):
        #print self.cleaned_data
        if self.cleaned_data['username'].lower() in userena_settings.USERENA_FORBIDDEN_USERNAMES:
            raise forms.ValidationError(_("This username is forbidden, select another one."))
        try:
            user = User.objects.filter(username__iexact=self.cleaned_data['username'])
            if user.count() > 0:
                raise forms.ValidationError(_("This username is already in use, select another one."))
        except User.DoesNotExist:
            pass
        return self.cleaned_data['username']


    def save(self):
        """ Creates a new user and account. Returns the newly created user. """
        logging.debug("In the new user .save()")
        username, email, password = (self.cleaned_data["username"],
                                     self.cleaned_data['email'], self.cleaned_data['password'])
        phone = self.cleaned_data['phone']
        first_name, last_name = (self.cleaned_data['first_name'],
                                 self.cleaned_data['last_name'])
        industry, website, expertise = (self.cleaned_data['industry'],
                                        self.cleaned_data['website'],
                                        self.cleaned_data['expertise'])


        kwargs = {"first_name" : first_name,
                  "last_name" : last_name,
                  "industry" : industry,
                  "password" : password,
                  "email" : email,
                  #"status" : "inactive",
                  "username" : username,
                  #"expertise" : expertise,
                  "phone" : phone,
                  }
        ep = ErinireProfile.create_user(**kwargs)
        return ep

class ErinireEditProfileForm(BetterModelForm):

    def __init__(self, *args, **kw):
        super(ErinireEditProfileForm, self).__init__(*args, **kw)
        # Put the first and last name at the top

    class Meta:
        model = ErinireProfile
        exclude = ['user', 'uuid']


class ErinireAuthenticationForm(BetterForm):
    username = forms.CharField( max_length=30, required=True,
        widget=forms.TextInput(), label=_("Username"),)

    password = forms.CharField(label=_("Password"), required = True,
        widget=forms.PasswordInput(render_value=False))
    #remember_me = forms.BooleanField(widget=forms.CheckboxInput(),
    #                                 required=False,
    #                                 label=_(u'Remember me for %(days)s') % {'days': _(userena_settings.USERENA_REMEMBER_ME_DAYS[0])})
    error_msg = _("Invalid Username or password.")
    def __init__(self, *args, **kwargs):
        """ A custom init because we need to change the label if no usernames is used """
        super(ErinireAuthenticationForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        try:
            user = User.objects.filter(username__iexact=self.cleaned_data['username'])
            if user.count < 1:
                logging.debug("user does not seem to exist.")
        except User.DoesNotExist:
            raise forms.ValidationError(self.error_msg)
        return self.cleaned_data['username']

    def clean(self):
        username = self.clean_username()
        logging.debug("Username passed")
        password = self.cleaned_data.get('password', '')
        logging.debug("Password passed")
        user = User.objects.filter(username__iexact=username)[0]
        if user.has_usable_password():
            logging.debug("Password is set in the profile!")
        else:
            logging.debug("Password not set in the user profile!")
        if user.check_password(password):
            logging.debug("Password matched user profile!")
        else:
            logging.debug("Password not matched user profile!")

        user = authenticate(username=username, password=password)
        if user is None:
            raise forms.ValidationError, _("Username or password is invalid.")
        return self.cleaned_data
