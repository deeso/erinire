import copy
from django import forms
from django.core.urlresolvers import reverse
from django.forms.widgets import RadioFieldRenderer
from django.forms.util import flatatt
from django.utils.html import escape
from django.utils.text import Truncator
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from django.utils.encoding import force_unicode
from django.forms.widgets import Input


JQ_DATE_PICKER ='''
<script>
    $(function() { $( ".datepicker" ).datepicker(); });
</script>'''

FILEUPLOAD = '''
<div class="row fileupload-buttonbar">
    <div class="span7">
        <span class="btn btn-primary fileinput-button">
            <i class="icon-plus icon-white"></i>
            <span>Add files...</span>
            <input type="file" name="file" multiple>
        </span>
        <button type="submit" class="btn btn-success start">
            <i class="icon-upload icon-white"></i>
            <span>Start upload</span>
        </button>
        <button type="reset" class="btn btn-warning cancel">
            <i class="icon-ban-circle icon-white"></i>
            <span>Cancel upload</span>
        </button>
        <button type="button" class="btn btn-danger delete">
            <i class="icon-trash icon-white"></i>
            <span>Delete files</span>
        </button>
        <input type="checkbox" class="toggle">
    </div>
    <div class="span5 fileupload-progress fade">
        <div class="progress progress-success progres-striped active">
            <div class="bar" style="width:0%"></div>
        </div>
        <div class="progress-extended">&nbsp;</div>
    </div>
</div>
<div class="fileupload-loading"></div>
<table class="table table-striped">
    <tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody>
</table>
'''
FILEUPLOAD_JS = '''
        <script>
            $(function(){
            var uploader = new qq.FileUploader({
                action: "submission_file_upload/",
                element: $('#attack-files')[0],
                multiple: true,
                onComplete: function(id, fileName, responseJSON) {
                    if(responseJSON.success) {
                        alert("success!");
                    } else {
                        alert("upload failed!");
                    }
                },
                onAllComplete: function(uploads) {
                    // uploads is an array of maps
                    // the maps look like this: {file: FileObject, response: JSONServerResponse}
                    alert("All complete!");
                },
                params: {
                    'csrf_token': '{{ csrf_token }}',
                    'csrf_name': 'csrfmiddlewaretoken',
                    'csrf_xname': 'X-CSRFToken',
                },
            });
            });
        </script>
'''

JQUERY_UPLOADER_JS = '''
<script>
    $('#file_upload').fileupload({
    url: 'php/index.php',
    done: function (e, data) {
        $.each(data.result, function (index, file) {
            $('<p/>').text(file.name).appendTo('body');
        });
    }
});
</script>
'''



class EmailInput(forms.EmailField): 
    input_type = 'email'

class TelephoneInput(Input): 
    input_type = 'tel'

class WebsiteInput(Input): 
    input_type = 'url'


class DateWidget(forms.DateInput):
    jQuery_code = JQ_DATE_PICKER
    input_type = 'date'
    def __init__(self, **kwargs):
        super(forms.DateInput, self).__init__(attrs={"size":10, "class":
            "datepicker", 'value':"02/16/12", 'readonly':'true','data-date-format':"mm/dd/yy" }, **kwargs)
    
    def render(self, name, value, attrs=None):
        out = super(DateWidget,self).render(name, value, attrs=attrs)
        return out + self.jQuery_code

    class Media:
        css = {"all":( "/static/attacksubmission/css/themes/trontastic/jquery.ui.all.css",)}
        js = ( "/static/js/jquery/jquery-ui-1.8.20.custom.min.js",)

        #css = {"all":("/static/attacksubmission/css/themes/base/jquery.ui.all.css",
        #"/static/attacksubmission/css/themes/trontastic/jquery.ui.all.css")}
        #js = ("/static/attacksubmission/js/jquery-1.7.2.js",
        #      "/static/js/jquery-ui-1.8.20.custom.min.js",)


class TextareaWidget(forms.Textarea):
    def __init__(self, attrs=None):
        final_attrs = {'class': 'vLargeTextField'}
        if attrs is not None:
            final_attrs.update(attrs)
        super(TextareaWidget, self).__init__(attrs=final_attrs)

  
POST_FILEUPLOAD_BSTRAP = '''<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
</div>'''
PRE_FILEUPLOAD_BSTRAP = '''<div class="fileupload fileupload-new" data-fileupload="file">
  <div class="fileupload-preview uneditable-input"></div>
  <span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" /></span>
  <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
</div>'''


QQ_UPLOADER_CLS = '''
<div id="%s">       
        <noscript>          
            <p>Please enable JavaScript to use file uploader.</p>
        </noscript>         
</div>'''

class FileUploadWidget(forms.FileInput):
    jQuery_code = ''#FILEUPLOAD_JS
    __pre = PRE_FILEUPLOAD_BSTRAP
    __post = POST_FILEUPLOAD_BSTRAP
    input_type = 'file'
    def __init__(self, id_tag='file-uploader', **kwargs):
        attrs={"size":10, 'id_tag':id_tag}
        print kwargs
        super(forms.FileInput, self).__init__(attrs=attrs, **kwargs)
    
    def render(self, name, value, attrs=None):
        out = super(FileUploadWidget,self).render(name, value, attrs=attrs)
        return QQ_UPLOADER_CLS%self.attrs['id_tag'] #out # self.__pre + out + self.jQuery_code

    class Media:
        __js_files = [ 
        #'fileuploader.js',
        ]
        #css = {"all":( "/static/attacksubmission/css/fileuploader/jquery.fileupload-ui.css",
        #               "/static/attacksubmission/css/fileuploader/style.css",)
        #      }
        #js = ["/static/js/fileuploader/"+i for i in __js_files]

