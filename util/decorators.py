
from django.conf import settings
from django.http import HttpResponsePermanentRedirect
from django.utils.decorators import available_attrs

from userena import settings as userena_settings
from userena.decorators import secure_required

from django.utils.functional import wraps

def secure_required_cls(view_func):
    def _wrapped_view(self, request, *args, **kwargs):
        if not request.is_secure():
            if userena_settings.USERENA_USE_HTTPS:
                request_url = request.build_absolute_uri(request.get_full_path())
                secure_url = request_url.replace('http://', 'https://')
                return HttpResponsePermanentRedirect(secure_url)
        return view_func(self, request, *args, **kwargs)
    return wraps(view_func, assigned=available_attrs(view_func))(_wrapped_view)
