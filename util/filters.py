
from coffin import template

def jinja_filter_contains(value, contains):
    return value.count(contains) > 0

register = template.Library()
register.filter('contains', jinja_filter_contains)
