from django.contrib.contenttypes import generic
from django.contrib.auth.models import User, Group
from django.db import models

from taggit.managers import TaggableManager

from phonenumber_field.modelfields import PhoneNumberField
from profile.baseerinire import ErinireObject
from profile.models import ErinireProfile


ASSIGNED_PERMISSIONS = {
    'profile':
        (('view_profile', 'Can view profile'),
         ('change_profile', 'Can change profile'),
         ('delete_profile', 'Can delete profile')),
    'user':
        (('change_user', 'Can change user'),
         ('delete_user', 'Can delete user'))
}


file_locker = "/tmp/"


class ErinireExtractedDataType(ErinireObject):
    desc = models.TextField()
    otype = models.CharField(max_length=50)
    created = models.DateTimeField()
    updated = models.DateTimeField()

class ErinireExtractedDataSource(ErinireObject):
    desc = models.TextField()
    otype = models.CharField(max_length=50)
    created = models.DateTimeField()
    updated = models.DateTimeField()

class ErinireGeneration(ErinireObject):
    desc = models.TextField()
    otype = models.CharField(max_length=50)
    created = models.DateTimeField()
    updated = models.DateTimeField()
    
class ErinireExtractedData(ErinireObject):
    desc = models.TextField()
    generation = models.ManyToManyField(ErinireGeneration)
    source = models.ManyToManyField(ErinireExtractedDataSource)
    otype = models.ForeignKey(ErinireExtractedDataType)
    created = models.DateTimeField()
    updated = models.DateTimeField()


class ErinireFileTypes(ErinireObject):
    desc = models.TextField()
    ext = models.CharField(max_length=50)
    otype = models.CharField(max_length=50)
    created = models.DateTimeField()
    updated = models.DateTimeField()


class ErinireFile(ErinireObject):
    name = models.TextField()
    actual_type = models.ForeignKey(ErinireFileTypes,
                  related_name='actual_type', )
           
    ext_type = models.ForeignKey(ErinireFileTypes,
                  related_name='ext_type', )
    desc = models.TextField(blank=True )
    #data = models.FileField(upload_to=file_locker)
    ck_sum = models.TextField()
    
    created = models.DateTimeField()
 
    location_type = models.CharField(max_length=10)
    location = models.CharField(max_length=500)
    tags = TaggableManager(blank = True)    

    def __unicode__(self):
        return self.name 

    def save(self, *args, **kwargs):
        # TODO take an MD5 of the file and get the type
        super(ErinireFile, self).save(*args, **kwargs)

class ErinireComment(ErinireObject):
    created = models.DateTimeField()
    updated = models.DateTimeField()
    author = models.ForeignKey(User)
    content = models.TextField()
    
    references = generic.GenericForeignKey('erinire_obj', 'uuid')
   

class ErinireGroup(ErinireObject):
    #Default class
    name = models.OneToOneField(Group, 
           unique=True, 
           related_name='group_profile', 
           verbose_name='group_profile')






    
class ErinireTools(ErinireObject):
    desc = models.TextField()
    url = models.CharField(max_length=200)
    name = models.CharField(max_length=50)
    tags = TaggableManager()
    recommendation = models.ManyToManyField(ErinireObject,
                            related_name="user_recommendations")
    
    

class ErinireNote(ErinireObject):
    title = models.CharField(max_length=100)
    content = models.TextField()
    files = models.ForeignKey(ErinireFile)
    
    tags = TaggableManager()    
    
    group = models.ForeignKey(ErinireGroup)
    author = models.ForeignKey(ErinireProfile)
    
    created = models.DateTimeField()
    

    
    def set_tags(self, tags):
        TaggableManager.objects.update_tags(self, tags)

    def get_tags(self, tags):
        return TaggableManager.objects.get_for_object(self)




class URLSubmission(models.Model):
    url= models.CharField(blank=True, max_length=500)
    browser_headers = models.TextField(blank=True)
    email_headers = models.TextField(blank=True)
    email_sender = models.TextField(blank=True)
    email_text = models.TextField(blank=True)

class SubmissionInformation(models.Model):
    name = models.CharField(blank=True, max_length=500)
    email = models.EmailField(blank=True,max_length=500)
    phone = PhoneNumberField(blank=True, )
    industry = models.CharField(max_length=500)
    attack_desc = models.TextField(blank=True)
    attack_date = models.DateTimeField()
    attack_impact = models.TextField(blank=True) 
    data_loss = models.TextField(blank=True) 
    attack_analysis = models.TextField(blank=True)
    attack_alerts  = models.TextField(blank=True)
    additional_details = models.TextField(blank=True)
    attack_files = models.FileField(upload_to="files", blank=True, )
    
    @models.permalink
    def get_absolute_url(self):
        return ('upload-new', )

    def save(self, *args, **kwargs):
        # TODO take an MD5 of the file and get the type and make sure this is the correct syntax
        super(ErinireFile, self).save(*args, **kwargs)

    

    list_display = ('industry', 'attack_date', 'name', 'email', 'phone',
                     'attack_desc', 
                     'attack_impact', 'data_loss', 'attack_analysis',
                     'attack_alerts', 'additional_details',
                     'attack_files')

    class Meta:
        db_table = "submissions"
        ordering = ['-attack_date']
     
    class Admin:
        list_display = ('industry', 'attack_date', 'name', 'email', 'phone',
                     'attack_desc', 
                     'attack_impact', 'data_loss', 'attack_analysis',
                     'attack_alerts', 'additional_details',
                     'attack_files')

        
    def __repr__(self):
        return self.industry + " "+ str(self.attack_date)


    def __unicode__(self):
        return self.industry + " "+ str(self.attack_date)


    
