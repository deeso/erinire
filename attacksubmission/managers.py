import logging
from django.db import models
from django.db.models import get_model
from django.db.models import Q
from django.contrib.auth.models import (SiteProfileNotAvailable, 
                           User, UserManager, 
                           Permission, AnonymousUser,) 
from django.contrib.contenttypes.models import ContentType

from userena.managers import UserenaManager
from userena.models import (UserenaSignup, UserenaBaseProfile, )


from guardian.shortcuts import assign, get_perms
import re, datetime

from hashlib import sha512


import erinire.settings as settings

