from django import forms
from django.utils.translation import ugettext as _

from form_utils.forms import BetterForm, BetterModelForm
from util.widgets import (TextareaWidget, DateWidget, TelephoneInput, FileUploadWidget)
from attacksubmission.models import (SubmissionInformation)


class SubmissionInformationForm(BetterModelForm):

    class Meta:
        model = SubmissionInformation
        fieldsets = [
                ('Industry and Date',               {'fields': ['industry', 'attack_date',]}),
                ('Contact Information', {'fields': ['name', 'email', 'phone',], 'classes': []}),
                ('Attack Information', {'fields': [     'attack_desc', 
                    'attack_impact', 'data_loss', 'attack_analysis',
                    'attack_alerts', 'additional_details'], 'classes': []}), 
                ('Files', {'fields': [     'attack_files', ]}), 
                ]
        widgets = {
                'phone':TelephoneInput(),
                'attack_date':DateWidget(),
                'attack_desc':TextareaWidget(),
                'attack_analysis':TextareaWidget(),
                'attack_alerts':TextareaWidget(),
                'attack_impact':TextareaWidget(),
                'attack_files':FileUploadWidget(id_tag='attack_files'),
                }

URL_RE = r'^http://[a-zA-Z0-9\-\.][a-zA-Z0-9\-\.]+\.[a-zA-Z0-9\-\.]$'



class ErinireFileReviewForm(BetterForm):
    desc = forms.CharField(widget=TextareaWidget())
    industry = forms.CharField( max_length=50, required = False,
            widget=forms.TextInput(),
            label=_("Industry"),
            error_messages={'invalid': _('Last name must contain only letters.')})
    files  = forms.FileField()
    class Meta:
        widgets = { "files":FileUploadWidget(id_tag="upload_files"),
                    "desc":TextareaWidget(),
                    }

