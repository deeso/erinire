from attacksubmission.models import SubmissionInformation
from django.contrib import admin

from django.contrib.localflavor import  *
from django.contrib.localflavor.us.forms import USPhoneNumberField


class SubmissionInformationAdmin(admin.ModelAdmin):
    #fields = ['industry', 'attack_date', 'name', 'email', 'phone',
    #                 'attack_desc', 
    #                 'attack_impact', 'data_loss', 'attack_analysis',
    #                 'attack_alerts', 'additional_details']
    fieldsets = [
        (None,               {'fields': ['industry', 'attack_date',]}),
        ('Contact Information', {'fields': ['name', 'email', 'phone',], 'classes': ['collapse']}),
        ('Attack Information', {'fields': [     'attack_desc', 
                     'attack_impact', 'data_loss', 'attack_analysis',
                     'attack_alerts', 'additional_details'], 'classes': ['collapse']}),

    ]
                
    
    
admin.site.register(SubmissionInformation, SubmissionInformationAdmin)
