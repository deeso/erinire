# Create your views here.
from hashlib import md5
import magic, os, traceback, logging, datetime
from io import FileIO, BufferedWriter

from attacksubmission.forms import (SubmissionInformationForm, )

from attacksubmission.models import (ErinireFileTypes,
                       ErinireFile )


from util.decorators import secure_required_cls
from util.baseview import BaseView, get, post
from erinire import settings
from django.core.context_processors import csrf
from django.shortcuts import redirect
from django.template import RequestContext

from coffin.shortcuts import render_to_response

from ajaxuploader.views import AjaxFileUploader
from ajaxuploader.backends.local import LocalUploadBackend









class MyLocalUploadBackend(LocalUploadBackend):
    UPLOAD_DIR =  getattr(settings, 'FILE_STORAGE', '/data/erinire/files')
    def setup(self, filename):
        self.name = filename
        try:
            self._path = os.path.join(self.UPLOAD_DIR,filename)
        except:
            logging.debug(traceback.print_exc())
        self._dest = BufferedWriter(FileIO(self._path, "w"))
        self._location_type = "local_fs"
        self._location = self._path

    def upload_complete(self, request, filename):
        path = self._path
        self._dest.close() 
        self._hash = ''
        try:
            self._hash = md5(open(self._path).read()).hexdigest()
            logging.debug("Hash: %s %s"%(self.name, self._hash))
        except:
            logging.debug(traceback.print_exc())
            raise
        try:
            created = datetime.datetime.now()
            updated = created
            _desc = magic.from_file(self._path)
            _type = _desc.split()[0].lower()
            _ext = os.path.splitext(self.name)[-1]
            logging.debug("Desc: %s\nType: %s\nExt: %s\n"%(_desc, _type,_ext))
            if len(_ext) > 1:
                _ext = _ext[-1]
            _ext= _ext.replace('.', '')
            
            ftypes = ErinireFileTypes.objects.filter(otype__iexact=_type)
            if ftypes.count < 1 :
                ftypes =ErinireFileTypes.objects.filter(desc__iexact=_desc) 
            

            ext_type = None
            etypes =ErinireFileTypes.objects.filter(ext__iexact=_ext) 
            if etypes.count() < 1:
                ext_type = ErinireFileTypes(desc=_desc,
                                         ext=_ext,
                                         otype=_type,
                                         created=created,
                                         updated=updated)
                ext_type.save()
            else:
                ext_type = etypes[0]

            logging.debug("Ext: got %s"%(ext_type.uuid))
            actual_type = None
            if ftypes.count() < 1:
                actual_type = ext_type
            else:
                actual_type = ftypes[0]
                logging.debug("Actual Ext: got %s"%(actual_type.uuid))
 
            
            
            erinire_file = ErinireFile(name = self.name,
                                       actual_type = actual_type,
                                       desc = _desc,
                                       location_type = self._location_type,
                                       location = self._path,
                                       ck_sum = self._hash,
                                       created = created,
                                       ext_type = ext_type )
              
            erinire_file.save()
        except:
            logging.debug(traceback.print_exc())
            raise

        return {"path": self._path, "hash":self._hash}

submission_uploader = AjaxFileUploader( backend = MyLocalUploadBackend)


class FilesReviewView(BaseView):
    @get
    @secure_required_cls
    def handle_get(self, request ):
        if not request.user.is_authenticated():
            return redirect('userena_signin')
        req_context = {'request':request,
                       'title': 'Erinire Files Review Page', 
                       'form': None}
        req_context.update(csrf(request))
        all_entries = [i for i in ErinireFile.objects.all()]
        
        req_context['files'] =  all_entries
        return render_to_response("erinire/filereview_form.html", req_context,
                 context_instance=RequestContext(request))

    @post
    @secure_required_cls
    def handle_post(self, request):
        if not request.user.is_authenticated():
            return redirect('userena_signin')
        req_context = {'request':request,
                       'title': 'Erinire Files Review Page', 
                       'form': None}
        req_context.update(csrf(request))
        all_entries = ErinireFile.objects.all()
        req_context['files'] =  all_entries
        return render_to_response("erinire/filereview_form.html", req_context,
                 context_instance=RequestContext(request))



class SubmissionView(BaseView):
    @get    
    @secure_required_cls
    def get_submission_page(self, request):
        if not request.user.is_authenticated():
            return redirect('userena_signin')
        submission_form = SubmissionInformationForm()
        req_context = {'request':request, 'title': 'Attack Submission Form', 'form': submission_form}
        req_context.update(csrf(request))
        return render_to_response('erinire/submission.html', req_context,
                context_instance=RequestContext(request))

    @post
    @secure_required_cls
    def handle_submission(self, request):
        if not request.user.is_authenticated():
            return redirect('userena_signin')

        if request.is_ajax():
            return self.handle_ajax_request(request)
        submission_form = SubmissionInformationForm(request.POST)
        print submission_form.is_valid()
        req_context = {'title': 'Attack Submission Form', 'form': submission_form}
        req_context.update(csrf(request))
        if request.FILES:
           attack_filenames = request.FILES.getlist('attack_files')
           self.handle_uploaded_file(attack_filenames)
        return render_to_response('erinire/submission.html', req_context,
                context_instance=RequestContext(request))

    def handle_uploaded_file(self, upfile):
        with open('/tmp/'+upfile.name, 'wb+') as destination:
            for chunk in upfile.chunks():
                destination.write(chunk)
        return True

    def handle_ajax_request(self, request):
        ufiles = request.FILES.getlist('attack_files')
        responses = []
        for attack_file_upload in ufiles:
            #lfile = MyLocalUploadBackend(attack_file_upload, basedir, request)
            #for c in attack_file_upload.chunks():
            #     lfile.recv_chunk(c)
            #responses.append( lfile.complete())
            pass
        return responses[0]     




