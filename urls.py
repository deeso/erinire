from coffin.conf.urls.defaults import *

from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from attacksubmission.views import SubmissionView, submission_uploader, FilesReviewView

from erinire import settings
# Uncomment the next two lines to enable the admin:

from userena import views as userena_views
from profile.forms import ErinireEditProfileForm
from profile.views import UserenaWrapperSignup, ActivateUser, SigninView
from urlsubmission.views import URLSubmissionView

admin.autodiscover()

urlpatterns = patterns('',
   url(r'ajax-upload$', submission_uploader, name="submission_upload"),
    url(r'^submit_attack/$', SubmissionView(), name='submission'),
    url(r'^submit_url/$', URLSubmissionView(), name='submit_url'),
    url(r'^submit_file/$', URLSubmissionView(), name='submit_file'),
    url(r'^update/$', SubmissionView(), name='update'),
    
    url(r'^files/', FilesReviewView(), name='filereview'),
    url(r'^messages/', include('userena.contrib.umessages.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url( r'^signup/$', UserenaWrapperSignup(), name='signup'),
    url( r'^(?P<username>[\.\w]+)/activate/(?P<activation_key>\w+)/$',
        ActivateUser(),
         name='userena_activate'),

    url(r'^signin/$',
       SigninView(),
       #{'template_name': 'attacksubmission/signin_form.html',
       # 'auth_form':ErinireAuthenticationForm},
       name='userena_signin'),
    url(r'^signout/$',
       userena_views.signout,
       name='userena_signout'),

    # Reset password
    url(r'^password/reset/$',
       auth_views.password_reset,
       {'template_name': 'userena/password_reset_form.html',
        'email_template_name': 'userena/emails/password_reset_message.txt'},
       name='userena_password_reset'),
    url(r'^password/reset/done/$',
       auth_views.password_reset_done,
       {'template_name': 'userena/password_reset_done.html'},
       name='userena_password_reset_done'),
    url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
       auth_views.password_reset_confirm,
       {'template_name': 'userena/password_reset_confirm_form.html'},
       name='userena_password_reset_confirm'),
    url(r'^password/reset/confirm/complete/$',
       auth_views.password_reset_complete,
       {'template_name': 'userena/password_reset_complete.html'}),
       
    # Change email and confirm it
    url(r'^(?P<username>[\.\w]+)/email/$',
       userena_views.email_change,
       name='userena_email_change'),
    url(r'^(?P<username>[\.\w]+)/email/complete/$',
       userena_views.direct_to_user_template,
       {'template_name': 'userena/email_change_complete.html'},
       name='userena_email_change_complete'),
    url(r'^(?P<username>[\.\w]+)/confirm-email/complete/$',
       userena_views.direct_to_user_template,
       {'template_name': 'userena/email_confirm_complete.html'},
       name='userena_email_confirm_complete'),
    url(r'^(?P<username>[\.\w]+)/confirm-email/(?P<confirmation_key>\w+)/$',
       userena_views.email_confirm,
       name='userena_email_confirm'),

    # Disabled account
    url(r'^(?P<username>[\.\w]+)/disabled/$',
       userena_views.direct_to_user_template,
       {'template_name': 'userena/disabled.html'},
       name='userena_disabled'),

    # Change password
    url(r'^(?P<username>[\.\w]+)/password/$',
       userena_views.password_change,
       name='userena_password_change'),
    url(r'^(?P<username>[\.\w]+)/password/complete/$',
       userena_views.direct_to_user_template,
       {'template_name': 'userena/password_complete.html'},
       name='userena_password_change_complete'),

    # Edit profile
    url(r'^(?P<username>[\.\w]+)/edit/$',
       userena_views.profile_edit,
        {'edit_profile_form': ErinireEditProfileForm,
        'template_name': 'erinire/profile_form.html',
        'extra_context':{'title':'Edit Profile'}}, 
        name='userena_profile_edit'),

    # View profiles
    url(r'^(?P<username>(?!signout|signup|signin)[\.\w]+)/$',
       userena_views.profile_detail,
       name='userena_profile_detail'),
    url(r'^page/(?P<page>[0-9]+)/$',
       userena_views.ProfileListView.as_view(),
       name='userena_profile_list_paginated'),
    url(r'^$',
       userena_views.ProfileListView.as_view(),
       name='userena_profile_list'),


     url(r'^assets/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_DOC_ROOT, 'show_indexes':True}),
)

