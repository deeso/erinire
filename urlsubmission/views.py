from coffin.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.shortcuts import redirect
from django.template.context import RequestContext
from attacksubmission.forms import SubmissionInformationForm
from attacksubmission.models import URLSubmission
from util.baseview import BaseView, get, post
from util.decorators import secure_required_cls

class URLSubmissionView(BaseView):
    @get
    @secure_required_cls
    def get_submission_page(self, request):
        if not request.user.is_authenticated():
            return redirect('userena_signin')
        submission_form = URLSubmission()
        req_context = {'request':request, 'title': 'URL Submission Form', 'form': submission_form}
        req_context.update(csrf(request))
        return render_to_response('erinire/urlsubmission.html', req_context,
            context_instance=RequestContext(request))

    @post
    @secure_required_cls
    def handle_submission(self, request):
        if not request.user.is_authenticated():
            return redirect('userena_signin')

        if request.is_ajax():
            return self.handle_ajax_request(request)
        submission_form = SubmissionInformationForm(request.POST)
        print submission_form.is_valid()
        req_context = {'title': 'Attack Submission Form', 'form': submission_form}
        req_context.update(csrf(request))
        if request.FILES:
            attack_filenames = request.FILES.getlist('attack_files')
            #self.handle_uploaded_file(attack_file)
        return render_to_response('erinire/urlsubmission.html', req_context,
            context_instance=RequestContext(request))


    def handle_uploaded_file(self, upfile):
        with open('/tmp/'+upfile.name, 'wb+') as destination:
            for chunk in upfile.chunks():
                destination.write(chunk)
        return True

    def handle_ajax_request(self, request):
        ufiles = request.FILES.getlist('attack_files')
        responses = []
        for attack_file_upload in ufiles:
            #lfile = MyLocalUploadBackend(attack_file_upload, basedir, request)
            #for c in attack_file_upload.chunks():
            #     lfile.recv_chunk(c)
            #responses.append( lfile.complete())
            pass
        return responses[0]
