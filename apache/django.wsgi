import os, sys, logging
#import django.core.handlers.wsgi

sys.path.insert(0,'/opt/dev-www/django_sites')
sys.path.insert(0,'/opt/dev-www/django_sites/erinire')

import settings

import django.core.management
django.core.management.setup_environ(settings)
utility = django.core.management.ManagementUtility()
command = utility.fetch_command('runserver')

command.validate()

import django.conf
import django.utils

django.utils.translation.activate(django.conf.settings.LANGUAGE_CODE)

import django.core.handlers.wsgi

import threading
import pprint
import time
import os

class LoggingInstance:
    def __init__(self, start_response, oheaders, ocontent):
        self.__start_response = start_response
        self.__oheaders = oheaders
        self.__ocontent = ocontent

    def __call__(self, status, headers, *args):
        pprint.pprint((status, headers)+args, stream=self.__oheaders)
        self.__oheaders.close()

        self.__write = self.__start_response(status, headers, *args)
        return self.write

    def __iter__(self):
        return self

    def write(self, data):
        self.__ocontent.write(data)
        self.__ocontent.flush()
        return self.__write(data)

    def next(self):
        data = self.__iterable.next()
        self.__ocontent.write(data)
        self.__ocontent.flush()
        return data

    def close(self):
        if hasattr(self.__iterable, 'close'):
            self.__iterable.close()
        self.__ocontent.close()

    def link(self, iterable):
        self.__iterable = iter(iterable)

class LoggingMiddleware:

    def __init__(self, application, savedir):
        self.__application = application
        self.__savedir = savedir
        self.__lock = threading.Lock()
        self.__pid = os.getpid()
        self.__count = 0

    def __call__(self, environ, start_response):
        self.__lock.acquire()
        self.__count += 1
        count = self.__count
        self.__lock.release()

        key = "%s-%s-%s" % (time.time(), self.__pid, count)

        iheaders = os.path.join(self.__savedir, key + ".iheaders")
        iheaders_fp = file(iheaders, 'w')

        icontent = os.path.join(self.__savedir, key + ".icontent")
        icontent_fp = file(icontent, 'w+b')

        oheaders = os.path.join(self.__savedir, key + ".oheaders")
        oheaders_fp = file(oheaders, 'w')

        ocontent = os.path.join(self.__savedir, key + ".ocontent")
        ocontent_fp = file(ocontent, 'w+b')

        errors = environ['wsgi.errors']
        pprint.pprint(environ, stream=iheaders_fp)
        iheaders_fp.close()

        length = int(environ.get('CONTENT_LENGTH', '0'))
        input = environ['wsgi.input']
        while length != 0:
            data = input.read(min(4096, length))
            if data:
                icontent_fp.write(data)
                length -= len(data)
            else:
                length = 0
        icontent_fp.flush()
        icontent_fp.seek(0, os.SEEK_SET)
        environ['wsgi.input'] = icontent_fp

        iterable = LoggingInstance(start_response, oheaders_fp, ocontent_fp)
        iterable.link(self.__application(environ, iterable))
        return iterable



application = django.core.handlers.wsgi.WSGIHandler()
application = LoggingMiddleware(application, '/opt/dev-www/django_sites//wsgi/')

sys.path.append('/opt/django_python/dev/lib/python2.7/site-packages/')
sys.path.append('/opt/django_python/dev/lib/python2.7/site-packages/django')
sys.path.append('/opt/django_python/dev/lib/python2.7/site-packages/ajaxuploader')
sys.path.append('/opt/django_python/dev/lib/python2.7/site-packages/guardian')
sys.path.append('/opt/django_python/dev/lib/python2.7/site-packages/userena')
sys.path.append('/opt/django_python/dev/lib/python2.7/site-packages/psycopg2')
sys.path.append('/opt/django_python/dev/lib/python2.7/site-packages/jinja2')
sys.path.append('/opt/django_python/dev/lib/python2.7/site-packages/form_utils')
sys.path.append('/opt/django_python/dev/lib/python2.7/site-packages/phonenumber_field')


#
#
#logging.error('Current system path is: %s'%"\n".join(sys.path))



#os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
#application = django.core.handlers.wsgi.WSGIHandler()

