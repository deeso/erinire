# Basically edit the configuration stuff in this file
# and rename it to sensitive_conf.py
#
# sensitive_conf.py
# Its used to store passwords, but git is set to ignore 
# it automatically so there is no unintentional leakage


email_user = ''
email_pass = ''
email_host = 'smtp.gmail.com'
email_port = 587
email_tls = True


# these configured when moving from one environment to the next
app_root = '/opt/dev-www/django_sites/erinire/'
logs_root = '/opt/dev-www/django_sites/erinire/'
virtual_env  = '/opt/dev-www/python_env/'
base_fs_store = '/Users/apridgen/Documents/git/txrxapp/media/files/'
admins = ('apridgen', 'adam.pridgen@thecoverofnight.com')


#db_user = 'django_dev'
#db_dbname = 'django_db_dev'
#db_password = ''
#db_engine = "django.db.backends.postgresql_psycopg2"
## using pgbouncer
#db_port = 6432
#db_host = "127.0.0.1"

#db_user = 'django_prod'
#db_dbname = 'django_db_prod'
#db_password = ''
#db_engine = "django.db.backends.postgresql_psycopg2"
#db_port = 5432
#db_host = "127.0.0.1"
