# Django settings for erinire project.

# this file is intentionally missing from the configuration.
# it contains the sensitive info
# admins, email_user, email_pass, app_root


import os, sys

from erinire import sensitive_conf as sconf

MANAGERS =  sconf.admins

DEPLOYMENT_BASE_ROOT = sconf.app_root
DEPLOYMENT_DEFAULT_FROM_EMAIL = sconf.email_user
DEPLOYMENT_EMAIL_HOST = sconf.email_host
DEPLOYMENT_EMAIL_HOST_USER = sconf.email_user
DEPLOYMENT_EMAIL_HOST_PASSWORD = sconf.email_pass
DEPLOYMENT_EMAIL_PORT = sconf.email_port
DEPLOYMENT_EMAIL_USE_TLS = sconf.email_tls


DEPLOYMENT_DB_HOST = sconf.db_host
DEPLOYMENT_DB_HOST_USER = sconf.db_user
DEPLOYMENT_DB_HOST_PASSWORD = sconf.db_password
DEPLOYMENT_DB_HOST_PORT = sconf.db_port
DEPLOYMENT_DB_NAME = sconf.db_dbname
DEPLOYMENT_DB_BACKEND = sconf.db_engine


DEBUG = True
TEMPLATE_DEBUG = DEBUG


UPLOAD_DIR = DEPLOYMENT_BASE_ROOT + "/data/attacksubmission/files/"

LOGIN_REDIRECT_URL = '/accounts/%(username)s/'
LOGIN_URL = '/accounts/signin/'
LOGOUT_URL = '/accounts/signout/'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

DEFAULT_FROM_EMAIL =  DEPLOYMENT_DEFAULT_FROM_EMAIL
EMAIL_HOST = DEPLOYMENT_EMAIL_HOST
EMAIL_HOST_USER = DEPLOYMENT_EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = DEPLOYMENT_EMAIL_HOST_PASSWORD
EMAIL_PORT = DEPLOYMENT_EMAIL_PORT
EMAIL_USE_TLS = DEPLOYMENT_EMAIL_USE_TLS


EMAIL_CONFIRMATION_DAYS = 2


# The user is required to hand over an e-mail address when signing up
ACCOUNT_EMAIL_REQUIRED = True

# After signing up, keep the user account inactive until the email
# address is verified
ACCOUNT_EMAIL_VERIFICATION = True

# Login by email address, not username
ACCOUNT_EMAIL_AUTHENTICATION = False

# Enforce uniqueness of e-mail addresses
ACCOUNT_UNIQUE_EMAIL = True

# Signup password verification
ACCOUNT_SIGNUP_PASSWORD_VERIFICATION = True

# Subject-line prefix to use for email messages sent
ACCOUNT_EMAIL_SUBJECT_PREFIX = "[eRiniRe]"

# Signup form
ACCOUNT_SIGNUP_FORM_CLASS = None

# The user is required to enter a username when signing up
ACCOUNT_USERNAME_REQUIRED = True

# render_value parameter as passed to PasswordInput fields
ACCOUNT_PASSWORD_INPUT_RENDER_VALUE = False




DATABASES = {
    'default': {
        'ENGINE': DEPLOYMENT_DB_BACKEND, # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': DEPLOYMENT_DB_NAME,                      # Or path to database file if using sqlite3.
        'USER': DEPLOYMENT_DB_HOST_USER,                      # Not used with sqlite3.
        'PASSWORD': DEPLOYMENT_DB_HOST_PASSWORD,                  # Not used with sqlite3.
        'HOST': DEPLOYMENT_DB_HOST,                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': DEPLOYMENT_DB_HOST_PORT,                      # Set to empty string for default. Not used with sqlite3.
    }
}


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

MEDIA_ROOT = DEPLOYMENT_BASE_ROOT + '/media/'
MEDIA_URL = '/media/'

STATIC_ROOT = DEPLOYMENT_BASE_ROOT + '/'
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'
STATIC_DOC_ROOT = STATIC_ROOT + STATIC_URL

# Additional locations of static files
STATICFILES_DIRS = (
    STATIC_ROOT + STATIC_URL,
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.messages.context_processors.messages',
    'django.contrib.auth.context_processors.auth',
)

AUTH_PROFILE_MODULE = 'profile.ErinireProfile'

ANONYMOUS_USER_ID = -1

AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',

)



MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'userena.middleware.UserenaLocaleMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    DEPLOYMENT_BASE_ROOT+"/templates/",
    DEPLOYMENT_BASE_ROOT+"/templates/attacksubmission/",
    DEPLOYMENT_BASE_ROOT+"/templates/ajaxuploader/",
    DEPLOYMENT_BASE_ROOT+"/templates/allauth/",
    DEPLOYMENT_BASE_ROOT+"/templates/userena/",
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
     #'admintools_bootstrap',
     #'admin_tools'
     'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    # file uploader
    'ajaxuploader',
    # authentication backend
   'guardian',
   'userena',
   # improved forms and model handling
   'form_utils',
   # tags in the site
   'taggit',
   'profile',
   'attacksubmission',
   'urlsubmission',


)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
       'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        }, 
        
        'default': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(sconf.logs_root, 'mylog.log'),
            'maxBytes': 1024*1024*5, # 5 MB
            'backupCount': 5,
            'formatter':'standard',
        },  
        'request_handler': {
                'level':'DEBUG',
                'class':'logging.handlers.RotatingFileHandler',
                'filename': os.path.join(sconf.logs_root, 'django_request.log'),
                'maxBytes': 1024*1024*5, # 5 MB
                'backupCount': 5,
                'formatter':'standard',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django.db': { # Stop SQL debug from logging to main logger
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False
        },

        '': {
            'handlers': ['default', 'console'],
            'level': 'DEBUG',
            'propagate': True
        },
        'django.request': { # Stop SQL debug from logging to main logger
            'handlers': ['request_handler'],
            'level': 'DEBUG',
            'propagate': False
        },
    }
}
JINJA2_EXTENSIONS = (
        'jinja2.ext.i18n',
        )
